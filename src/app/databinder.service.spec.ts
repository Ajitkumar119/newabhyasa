import { TestBed, inject } from '@angular/core/testing';

import { DatabinderService } from './databinder.service';

describe('DatabinderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatabinderService]
    });
  });

  it('should be created', inject([DatabinderService], (service: DatabinderService) => {
    expect(service).toBeTruthy();
  }));
});
