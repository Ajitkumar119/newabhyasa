import { Injectable } from '@angular/core';
import {HttpModule,Http,Response} from'@angular/http';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class DatabinderService {
  http: Http;
  addurl: string;
  zoneurl: string;
  companyurl: string;


  constructor(public _http:Http) {
  this.http = _http;
  this.addurl = "https://meghak.divrt.co/api/v3/provider/login";
  this.zoneurl = "https://meghak.divrt.co/api/v3/zone/getPrimeZoneListBylatlng";
  this.companyurl = "https://meghak.divrt.co/api/v3/zone/getCompanyList";


   }
  datafunction(datainput){
    return this.http.post(this.addurl , datainput)
      .pipe(map((res: any) => res.json()));
  }
  zonelist(){
  	return this.http.post(this.zoneurl , {})
      .pipe(map((res: any) => res.json()));
  }
  company(){
  	return this.http.post(this.companyurl , {})
      .pipe(map((res: any) => res.json()));
  }
  zone(){
  	return this.http.post(this.zoneurl , {})
      .pipe(map((res: any) => res.json()));
  }
  // zone(){
  // 	return this.http.post(this.zoneurl , {})
  //     .pipe(map((res: any) => res.json()));
  // }
}
