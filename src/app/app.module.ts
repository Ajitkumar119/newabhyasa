import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from'@angular/http';


import { AppComponent } from './app.component';
import {  FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {SlideshowModule} from 'ng-simple-slideshow';

@NgModule({
  declarations: [
    AppComponent		
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    SlideshowModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
