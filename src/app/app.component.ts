import { Component,OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import * as $ from "jquery";
import {DatabinderService} from './databinder.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DatabinderService]
})
export class AppComponent implements OnInit {
  title = 'app';
  user={};
  visibility1=false;
  visibility2=false;
  visibility3=false;
  userlog=undefined;
  userslog=undefined;
  users=[];
  tablevisible=false;
  vehicletable=false;
  zones=[];
  vehicles=[];
  companies=[];

  imageUrlArray=[];
  constructor(private databinder : DatabinderService){};

 ngOnInit(){
      //     $(document).ready(function() {
      //     $('.flexslider').flexslider({
      //     animation: "slide",
      //     reverse: 'true'
      //   });
      // });
      // this.databinder.zone().subscribe(
      // datareceive=>{
      //   console.log(datareceive);
      //   this.zones=datareceive.zones;
      //   console.log(this.zones);
      //    },
      // err => console.log(err),
      // () => console.log('Request Completed')
      // )

      this.databinder.company().subscribe(
      datareceive=>{
        console.log(datareceive);
        this.companies=datareceive.data;
        console.log(this.companies);

        // if(datareceive.status == true){
        //     alert('tfct')
        // }


      },
      err => console.log(err),
      () => console.log('Request Completed')
      )

       this.databinder.company().subscribe(
      datareceive=>{
        console.log(datareceive);
        this.vehicles=datareceive.data;
        console.log(this.vehicles);
         },
      err => console.log(err),
      () => console.log('Request Completed')
      )
       this.databinder.zonelist().subscribe(
      datareceive=>{
        console.log(datareceive);
        this.zones=datareceive.zones;
        

        // if(datareceive.status == true){
        //     alert('tfct')
        // }


      },
      err => console.log(err),
      () => console.log('Request Completed')
      )
      this.imageUrlArray=["../../assets/image/image1.jpg","../../assets/image/image2.jpg","../../assets/image/image3.jpg"];
 }
  onsignup(){
  	console.log(this.user);
  }

  onlogin(){
  	console.log(this.user);
    this.userlog=this.user['userid'];
    this.users.push(this.user);
    console.log(this.users);
    //this.user={};
    this.tablevisible=true;
    this. databinder.datafunction(this.user).subscribe(
      datareceive=>{
        console.log(datareceive);
      },
      err => console.log(err),
      () => console.log('Request Completed')
      )
      
  }
  onapply(){
    this.vehicletable=true;
  }
  signupfrm(){
  	this.visibility1=true;
  	this.visibility2=false;
  }
  Loginfrm(){
  	this.visibility2=true;
  	this.visibility1=false;
    // this.userlog=this.user['userid'];

  }
  alertpopup(){
    this.visibility3=true;
  }
  onsignin(){
    console.log(this.user);
    this.userslog=this.user['signinid'];
    this.visibility3=false;
  }
  onevent(e){
    console.log(e)
  }
}


